#!/bin/bash
set -e

cd ..
# clone deployment playbook
git clone git@bitbucket.org:samyunwan/ansible.git
cd ansible
# install playbook requirements
chmod +x *
# install playbook role requirements
timeout 9m ./start.sh
cd ..
rm -rf ansible
# setup vault password retrieval from travis envvar
echo $ANSIBLE_PLAYBOOK_PASSWORD > ~/.vault
#chmod +x ~/.vault

# deploy to qa using ansible-playbook
echo "Running: pipenv run ansible-playbook -i inventory/prod playbook.yml --vault-password-file=~/.vault -e rails_app_git_branch=$CIRCLE_TAG"
pipenv run ansible-playbook -i inventory/prod playbook.yml --vault-password-file=~/.vault -e rails_app_git_branch=$CIRCLE_TAG
